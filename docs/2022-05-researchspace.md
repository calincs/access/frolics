---
id: 2022-05-researchspace
title: "LINCS ResearchSpace Home Pages and Project Pages"
slug: /researchspace-may2022
draft: true
---

:::info

- Date: May 24, 2022
- Start Time: 9:00 AM
- Location: Online

:::

### Frolic Focus

<!-- Insert some words here -->

### Participants

Organized by Rashmeet Kaur, Kathleen McCulloch-Cop, and Kim Martin.

#### Group 1

- Alliyya
- Sarah Roger
- Jingyi
- Basil Yusuf

#### Group 2

- Jessica
- Hannah Stewart
- Kate

#### Group 3

- Ibrahim
- Sam
- Evan
- Amelia

### Designs
<!-- Update images below -->
#### Group 1
![Group 1 Design](/img/june2021_gr1.png)
#### Group 2
![Group 2 Design](/img/june2021_gr2.png)
#### Group 3
![Group 3 Design](/img/june2021_gr3.png)
