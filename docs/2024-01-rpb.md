---
id: 2024-01-rpb
title: "Data Dreams and Beyond the Bubbles"
description: "January 20, 2024: Dream of data with us"
slug: /data-dreams-jan2024
draft: true
---

:::info

- Date: January 20, 2024
- Start Time: 10:00 AM
- Location: THINC Lab

:::

## Researcher Questions

1. What process do researchers and people interested in cultural data use to organise and create understanding from large amounts of data/information?
1. How can [LOD](https://lincsproject.ca/docs/terms/linked-open-data) be used in a browsing environment to meet the needs of researchers?

## Frolic Focus

The goal of this design frolic is to get us thinking about browsing, with the specific intention of finessing the interface of a rich-prospect environment for browsing linked open data. In the morning, participants will be asked to work in small groups to consider a series of objects and the questions they would ask about them, given their own interests and unique points of view. From there, the LINCS UX team will lead the groups through an exercise to develop metadata about their items.

After lunch, we’ll regroup and do a brief introduction to the 3D browsing tool LINCS is developing and the groups will be asked to brainstorm how their information needs from the morning activity could be visualised in this interface. All groups will present back their thoughts and a final summary of possibilities will be discussed and detailed on this site as follow up.

## UX Team

- Robin Bergart
- Jordan Lum
- Kim Martin
- Alliyya Mo
- Susan Brown
