---
title: Past + Future Frolics
slug: /
sidebar_position: 1
---

## Interested in Frolicking?

Keep an eye on these pages for details about past and upcoming design frolics!

## Upcoming:

- February 2025: Contextual Exploration

### Contact us

Want to participate? Got a design problem you want to have frolic about?
You can get in touch with Kim Martin through [her website](https://www.kim-martin.ca/)
