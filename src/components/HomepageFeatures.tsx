import React from "react";
import clsx from "clsx";

interface FeatureItem {
  description: string;
}

const FeatureList: FeatureItem[] = [
  {
    description: `From the Amish tradition, a frolic is traditionally a large gathering
      with the aim to solve a problem, foster community, and enjoy yourselves.
      Design frolics are just that: a chance to collaborate, explore, play,
      and get creative.`,
  },
  {
    description: `Each is focused around a specific prompt: a design problem or concept
      that could benefit from brainstorming and collaborative idea generation.`,
  },
  {
    description: `This is a time to have fun, explore, try out new things, and work with
        others to try to solve some design problems. There's no pressure, no
        competition, and no expectation; just an opportunity for people to play
        around and try to design something new.`,
  },
];

function Feature({ description }: FeatureItem) {
  return (
    <div className={clsx("col col--4")}>
      <p>{description}</p>
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section>
      <div className="container">
        <h1 className="label">What's a Design Frolic?</h1>
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
