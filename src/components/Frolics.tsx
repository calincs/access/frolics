import React from "react";
import Link from "@docusaurus/Link";
import frolicsData from "@site/static/frolicData.json";

interface FrolicItem {
  name?: string;
  date?: string;
  colourCode: string;
  link?: string;
}

const futureFrolics: FrolicItem[] = frolicsData.futureFrolics;
const pastFrolics: FrolicItem[] = frolicsData.pastFrolics;

function Frolic({ name, date, colourCode, link }: FrolicItem): JSX.Element {
  return (
    <div className={"col paintchip"}>
      <Link to={link}>
        <p>{name}</p>
        <div className="colourcode">
          <span>{date}</span>
          <span>{colourCode}</span>
        </div>
      </Link>
    </div>
  );
}

function Frolics(frolics: FrolicItem[]): JSX.Element {
  return (
    <div className="row">
      {frolics.map((props, idx) => (
        <Frolic key={idx} {...props} />
      ))}
    </div>
  );
}

interface FrolicListProps {
  label: string;
  frolics: FrolicItem[];
}

function FrolicList({ label, frolics }: FrolicListProps): JSX.Element {
  return (
    <section>
      <div className="container">
        <h1 className="label">{label}</h1>
        <div className="row">
          {frolics.map((props, idx) => (
            <Frolic key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
export default function HomepageFrolics(): JSX.Element {
  return (
    <>
      <FrolicList label="Upcoming Frolics" frolics={futureFrolics} />
      <FrolicList label="Past Frolics" frolics={pastFrolics} />
    </>
  );
}
