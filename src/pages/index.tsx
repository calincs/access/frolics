import React from "react";
import Layout from "@theme/Layout";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import HomepageFeatures from "../components/HomepageFeatures";
import Frolics from "../components/Frolics";

export default function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout>
      <div>
        <div className="logo">
          <img src="img/designfrolicLogo.jpg" />
        </div>
        <HomepageFeatures />
        <Frolics />
      </div>
    </Layout>
  );
}
