FROM node:18-bullseye

# Default values for env vars to pass build test
RUN mkdir -p /home/node/app/node_modules \
  && chown -R node:node /home/node/app
WORKDIR /home/node/app
USER node
COPY --chown=node:node . .
RUN npm install 


RUN npm run build
EXPOSE 3000

CMD ["npm", "run", "serve"]
