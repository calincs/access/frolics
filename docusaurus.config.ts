import { themes as prismThemes } from "prism-react-renderer";
import type { Config } from "@docusaurus/types";
import type * as Preset from "@docusaurus/preset-classic";

const config: Config = {
  title: "Design Frolics",
  tagline: "Frolic with LINCS",
  url: "https://frolics.lincsproject.ca",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",
  organizationName: "LINCS SCALE", // Usually your GitHub org/user name.
  projectName: "frolics", // Usually your repo name.
  i18n: {
    defaultLocale: "en",
    locales: ["en", "fr"],
    localeConfigs: {
      en: {
        label: "English",
      },
      fr: {
        label: "Français",
      },
    },
  },
  themeConfig: {
    navbar: {
      title: "Design Frolics",
      logo: {
        alt: "Design Frolic",
        src: "img/designfrolicLogo.jpg",
      },
      items: [
        {
          to: "docs/",
          activeBasePath: "docs",
          label: "Past Frolics",
          position: "left",
        },
      ],
    },
    footer: {
      style: "light",
      links: [
        {
          title: "Contact Us",
          items: [
            {
              label: "Kim-Martin.ca",
              to: "https://www.kim-martin.ca/",
            },
            {
              label: "LINCS",
              to: "https://lincsproject.ca/",
            },
            {
              label: "GitLab",
              to: "https://gitlab.com/calincs/access/frolics",
            },
          ],
        },
        {
          title: "Frolics",
          items: [
            {
              label: "All Past Frolics",
              to: "/docs/",
            },
          ],
        },
      ],
      copyright: `Created by Kim Martin, Rashmeet Kaur, and Kathleen McCulloch-Cop. Made possible by SCALE and LINCS.`,
    },
  } satisfies Preset.ThemeConfig,
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          sidebarPath: require.resolve("./sidebars.ts"),
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      } satisfies Preset.Options,
      
    ],
  ],
};
export default config;
